octopus:
	python3 octopus.py

espresso:
	python3 espresso.py

nwchem:
	python3 nwchem.py

clean:
	rm -rf work
